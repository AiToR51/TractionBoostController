#include <Arduino.h>
#include "VssSensorManager/VssSensorManager.h"
#include "utils.c"
#include "I2Cdev/I2Cdev.h"
#include "GUI/GUI.h"
#include "MPU6050/MPU6050.h"
#include <Wire.h>

#define VSS_FRONT_LEFT_SENSOR_PIN 14
#define VSS_FRONT_RIGHT_SENSOR_PIN 15
#define VSS_REAR_LEFT_SENSOR_PIN 16
#define VSS_REAR_RIGHT_SENSOR_PIN 17

#define VSS_SENSOR_10M_PULSES 150

static const unsigned long MIN_ENGINE_RPM_EVENT_PERIOD_MICROS = 1000000; //1000 rpm = 62500 micros / event

enum TractionWorkMode {
	Disabled, Enabled
};

enum BoostConfig {
	Low, Medium, High
};

VssSensorManager *vssSensorManager;

TractionWorkMode tractionWorkMode;
BoostConfig boostConfig;

#define TABLE_X_AXIS_LENGTH 8
//#define TABLE_Y_AXIS_LENGTH 6

#define CHICHO
//#define AITOR

#define GEAR_SELECTION_ERROR 15

 unsigned short table[TABLE_X_AXIS_LENGTH] = {

 #ifdef CHICHO
 #include "tables/chicho/table.h"
 #endif
 #ifdef AITOR
 #include "tables/test1/table.h"
 #endif
 };

 unsigned int xAxis[TABLE_X_AXIS_LENGTH] = {
 #ifdef CHICHO
 #include "tables/chicho/axis/XAxis.h"
 #endif
 #ifdef AITOR
 #include "tables/test1/axis/XAxis.h"
 #endif
 };

 unsigned short gears[6] = {
 #ifdef CHICHO
 #include "tables/rpm/chicho/gearAxis.h"
 #endif
 };

static const unsigned long RPM_ENGINE_PERIOD_CHECKING_MS = 150;
unsigned long lastRPMEngineCheckTime = 0;
unsigned int engineRPMPulseCounter = 0;

static const unsigned long TRACTION_PERIOD_CHECKING_MS = 500;
unsigned long lastTractionCheckTime = 0;

#define SCREEN_UPDATE_PERIOD 100
unsigned long screenUpdateTime = 0;
unsigned long lastEngineRPMEvent = 0;
unsigned long previousEngineRPMEvent = 0;

unsigned int currentEngineRPM = 0;
int currentGear = -1;

GUI gui;
MPU6050 accelgyro;
int16_t ax, ay, az;

/*Control de interrupciones*/

void interruptFL() {
	vssSensorManager->interruptFL();
}
void interruptFR() {
	vssSensorManager->interruptFR();
}
void interruptRL() {
	vssSensorManager->interruptRL();
}
void interruptRR() {
	vssSensorManager->interruptRR();
}

void configureInterrupts() {
	pinMode(VSS_FRONT_LEFT_SENSOR_PIN, INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(VSS_FRONT_LEFT_SENSOR_PIN),
			interruptFL, FALLING);
	pinMode(VSS_FRONT_RIGHT_SENSOR_PIN, INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(VSS_FRONT_RIGHT_SENSOR_PIN),
			interruptFR, FALLING);
	pinMode(VSS_REAR_LEFT_SENSOR_PIN, INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(VSS_REAR_LEFT_SENSOR_PIN),
			interruptRL, FALLING);
	pinMode(VSS_REAR_RIGHT_SENSOR_PIN, INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(VSS_REAR_RIGHT_SENSOR_PIN),
			interruptRR, FALLING);
}


void setup() {
	delay(100);
	Serial.begin(115200);
	pinMode(13, OUTPUT);
	digitalWrite(13, HIGH);
	delay(1000);
	digitalWrite(13, LOW);
	delay(1000);
	digitalWrite(13, HIGH);
	delay(1000);
	digitalWrite(13, LOW);
	delay(1000);
	tractionWorkMode = Disabled;
	boostConfig = Low;


	gui.printFirstScreen();
	delay(1000);
	gui.prepareStructures();
	configureInterrupts();
	vssSensorManager = new VssSensorManager(
			new VssSensor(VSS_FRONT_LEFT_SENSOR_PIN, VSS_SENSOR_10M_PULSES),
			new VssSensor(VSS_FRONT_RIGHT_SENSOR_PIN, VSS_SENSOR_10M_PULSES),
			new VssSensor(VSS_REAR_LEFT_SENSOR_PIN, VSS_SENSOR_10M_PULSES),
			new VssSensor(VSS_REAR_RIGHT_SENSOR_PIN, VSS_SENSOR_10M_PULSES));
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
	Wire.begin();
#elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
	Fastwire::setup(400, true);
#endif

	accelgyro.initialize();
}

int getCurrentGear(unsigned int currentRPM, unsigned int currentSpeed) {
	int bestGear = -1;
	double bestGearError = 100;
	for (int i = 0; i < 6; i++) {
		int gearError = abs(
				100 - (((currentRPM / 10) * gears[i]) / currentSpeed));

		if (gearError < bestGearError) {
			bestGearError = gearError;
			bestGear = i;
		}
	}

	return bestGear;
}

int getBaseBoost(unsigned int currentRPM, unsigned int currentSpeed,
		int currentGear) {
	//validate rpm are valid
	int rpm4Calcs = 4000;
	if (currentRPM > 500) {
		rpm4Calcs = currentRPM;
	}
	if (currentGear >= 0) {
		//recuperamos soplado por marcha
	} else {
		//recuperamos soplado de marcha X(3);
	}
	return 0;
}

void setBoost(int boost) {
	//TODO
}


void updateScreen() {
	gui.getLCD()->print("x", LEFT, 10);
	gui.getLCD()->print("y", LEFT, 30);
	gui.getLCD()->print("z", LEFT, 50);

	gui.getLCD()->printNumI(ax, RIGHT, 10, 7, '0');
	gui.getLCD()->printNumI(ay, RIGHT, 30, 7, '0');
	gui.getLCD()->printNumI(az, RIGHT, 50, 7, '0');

	gui.getLCD()->print("FL", LEFT, 70);
	gui.getLCD()->print("FR", LEFT, 90);
	gui.getLCD()->print("RL", LEFT, 110);
	gui.getLCD()->print("RR", LEFT, 130);
	gui.getLCD()->printNumI(vssSensorManager->getFrontSpeedKmh(true), RIGHT, 70,
			5, '0');
	gui.getLCD()->printNumI(vssSensorManager->getFrontSpeedKmh(false), RIGHT,
			90, 5, '0');
	gui.getLCD()->printNumI(vssSensorManager->getRearSpeedKmh(true), RIGHT, 110,
			5, '0');
	gui.getLCD()->printNumI(vssSensorManager->getRearSpeedKmh(false), RIGHT,
			130, 5, '0');
	gui.getLCD()->printNumI(accelgyro.getDeviceID(), CENTER, 150, 5, '0');

	Serial.print("FL ");
	Serial.println(vssSensorManager->getFrontSpeedKmh(true));
	Serial.print("FR ");
	Serial.println(vssSensorManager->getFrontSpeedKmh(false));
	Serial.print("RL ");
	Serial.println(vssSensorManager->getRearSpeedKmh(true));
	Serial.print("R ");
	Serial.println(vssSensorManager->getRearSpeedKmh(false));

}

void calcRPM(unsigned long lastRPMEvent, unsigned long previousRPMEvent,
		unsigned int * currentRPM, unsigned long minPeriod) {
	noInterrupts();
	if ((micros() - lastRPMEvent) > minPeriod || previousRPMEvent == 0) {
		*currentRPM = 0;
	} else {
		*currentRPM = 60000000L / (lastRPMEvent - previousRPMEvent);
	}
	interrupts();
}

void loop() {
	accelgyro.getAcceleration(&ax, &ay, &az);

//	if (cycleCheck(&lastRPMEngineCheckTime, RPM_ENGINE_PERIOD_CHECKING_MS)) {
//		calcRPM(lastEngineRPMEvent, previousEngineRPMEvent, &currentEngineRPM,
//				MIN_ENGINE_RPM_EVENT_PERIOD_MICROS);
//		currentEngineRPM = currentEngineRPM * 2;
//		unsigned int frontSpeed = vssSensorManager->getFrontSpeedKmh();
//
//		currentGear = getCurrentGear(currentEngineRPM, frontSpeed);
//
//	}
//
//	if (cycleCheck(&lastTractionCheckTime, TRACTION_PERIOD_CHECKING_MS)) {
//		//soplado base = por marcha + rpm;
//		unsigned int frontSpeed = vssSensorManager->getFrontSpeedKmh();
//		int baseBoost = getBaseBoost(currentEngineRPM, frontSpeed, currentGear);
//
//		if (tractionWorkMode == Enabled) {
//			//ver si hay que recortar.
//		}
//
//		setBoost(baseBoost);
//	}
//
	if (cycleCheck(&screenUpdateTime, SCREEN_UPDATE_PERIOD)) {
		updateScreen();
	}
}

