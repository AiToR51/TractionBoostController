
#ifndef VssSensor_h
#define VssSensor_h

#include <Arduino.h>


class VssSensor {
public:
	~VssSensor();
	VssSensor(unsigned int sensorPin, unsigned long pulses10Meters);
	unsigned int getSpeedKmh();
	void calibrationMode();
	void interrupt();
private:
	unsigned long _pulses10Meters;
	unsigned long _vssSensorInterruptCount;
	unsigned int _sensorPin;
	void resetInterruptCount();

};

#endif
