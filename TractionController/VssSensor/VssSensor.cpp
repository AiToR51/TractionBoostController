#include "VssSensor.h"


void test(int a) {

}

VssSensor::VssSensor(unsigned int sensorPin, unsigned long pulses10Meters) {
	_sensorPin = sensorPin;
	_pulses10Meters = pulses10Meters;
	_vssSensorInterruptCount = 0;

}

unsigned int VssSensor::getSpeedKmh() {
	//calcular velocidad cada X tiempo.
	return _vssSensorInterruptCount;

}



void VssSensor::resetInterruptCount() {
	noInterrupts();
	_vssSensorInterruptCount = 0L;
	interrupts();
}

void VssSensor::interrupt() {
	_vssSensorInterruptCount++;
}

// the destructor frees up allocated memory
VssSensor::~VssSensor() {

}
