#ifndef GUI_GUI_H_
#define GUI_GUI_H_

#include <Arduino.h>
#include "../UTFT/UTFT.h"
#include "../UTFT/DefaultFonts.h"


class GUI {
public:
	~GUI();
	GUI();
	void printFirstScreen();
	void prepareStructures();
	UTFT* getLCD();
private:
	UTFT* myGLCD;
};

#endif /* GUI_GUI_H_ */
