#include "VssSensorManager.h"


#define MIN_SPEED 4

VssSensorManager::VssSensorManager(VssSensor *frontLeftSensor,
		VssSensor *frontRightSensor, VssSensor *rearLeftSensor,
		VssSensor *rearRightSensor) {
	_frontLeftSensor = frontLeftSensor;
	_frontRightSensor = frontRightSensor;
	_rearLeftSensor = rearLeftSensor;
	_rearRightSensor = rearRightSensor;
}

void VssSensorManager::interruptFL() {
	_frontLeftSensor->interrupt();
}
void VssSensorManager::interruptFR() {
	_frontRightSensor->interrupt();
}
void VssSensorManager::interruptRL() {
	_rearLeftSensor->interrupt();
}
void VssSensorManager::interruptRR() {
	_rearRightSensor->interrupt();
}

unsigned int VssSensorManager::getFrontSpeedKmh() {
	return getSpeedKmh(_frontLeftSensor->getSpeedKmh(),
			_frontRightSensor->getSpeedKmh());
}

unsigned int VssSensorManager::getRearSpeedKmh() {
	return getSpeedKmh(_rearLeftSensor->getSpeedKmh(),
			_rearRightSensor->getSpeedKmh());
}

unsigned int VssSensorManager::getFrontSpeedKmh(bool left) {
	return left ?
			_frontLeftSensor->getSpeedKmh() : _frontRightSensor->getSpeedKmh();
}

unsigned int VssSensorManager::getRearSpeedKmh(bool left) {
	return left ?
			_rearLeftSensor->getSpeedKmh() : _rearRightSensor->getSpeedKmh();
}

unsigned int VssSensorManager::getSpeedKmh(unsigned int leftSpeed,
		unsigned int rightSpeed) {
	unsigned int speed = 0;

	if (leftSpeed > MIN_SPEED) {
		speed = leftSpeed;
	}
	if (rightSpeed > MIN_SPEED) {
		if (speed == 0) {
			speed = rightSpeed;
		} else {
			speed = (speed + rightSpeed) / 2;
		}
	}
	return speed;

}

int VssSensorManager::getSpeedDifferencePercent() {
	unsigned int frontSpeed = getFrontSpeedKmh();
	unsigned int rearSpeed = getRearSpeedKmh();
	return (100 * (rearSpeed - frontSpeed)) / frontSpeed;
}

int VssSensorManager::getSpeedDifferenceAbs() {
	unsigned int frontSpeed = getFrontSpeedKmh();
	unsigned int rearSpeed = getRearSpeedKmh();
	return rearSpeed - frontSpeed;
}

void VssSensorManager::calibrationMode() {
	_frontLeftSensor->calibrationMode();
	_frontRightSensor->calibrationMode();
	_rearLeftSensor->calibrationMode();
	_rearRightSensor->calibrationMode();
}

// the destructor frees up allocated memory
VssSensorManager::~VssSensorManager() {

}
