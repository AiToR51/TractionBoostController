#ifndef VssSensorManager_h
#define VssSensorManager_h

#include <Arduino.h>
#include "../VssSensor/VssSensor.h"

class VssSensorManager {
public:
	~VssSensorManager();
	VssSensorManager(VssSensor *frontLeftSensor, VssSensor *frontRightSensor,
			VssSensor *rearLeftSensor, VssSensor *rearRightSensor);
	unsigned int getFrontSpeedKmh();
	unsigned int getRearSpeedKmh();
	unsigned int getFrontSpeedKmh(bool left);
	unsigned int getRearSpeedKmh(bool left);
	int getSpeedDifferencePercent();
	int getSpeedDifferenceAbs();
	void calibrationMode();
	void interruptFL();
	void interruptFR();
	void interruptRL();
	void interruptRR();
private:
	VssSensor *_frontLeftSensor;
	VssSensor *_frontRightSensor;
	VssSensor *_rearLeftSensor;
	VssSensor *_rearRightSensor;
	unsigned int getSpeedKmh(unsigned int leftSpeed, unsigned int rightSpeed);
	
};

#endif
